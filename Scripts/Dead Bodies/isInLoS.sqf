/* ----------------------------------------------------------------------------------------------------
 * Find if an object is in the Line-of-Sight of a unit. Only single objects are supported (no arrays).
 * The scipt was inspired from a video by Feuerex.
 * ----------------------------------------------------------------------------------------------------
 * Author: Achilles.
 * ----------------------------------------------------------------------------------------------------
 * Input
 * ----------
 * unit [Object]: The unit whose FoV to be calculated.
 * target [Object]: What the unit looks for.
 * fov [Number] (Optional): Angle of FoV of the unit in degrees (defaults to 90 - i.e. ±45).
 * threshold [Number] (Optional): Number to threshold the amount of visibility. The script
 *                     internally uses a command that returns a number from 0 to 1 to check for
 *                     visibility (checkVisibility). This number can be used to threshold the result.
 *                     It is in the range [0, 1] (defaults to 0).
 * ----------------------------------------------------------------------------------------------------
 * Output
 * ----------
 * inLoS [Boolean]: Whether the object is visible.
 * ----------------------------------------------------------------------------------------------------
 * How-To:
 * ----------
 * Call the script as a function.
 * ----------------------------------------------------------------------------------------------------
 * Example
 * ----------
 * Check whether an enemy unit is in the LoS of a friendly. If yes, send for reinforcements.
 *
 *   _visible = [_friendlyUnit, _enemyUnit, 80, 0.3] call compile preprocessFileLineNumbers "isInLoS.sqf";
 *   if(_visible) then {
 *     sideChat ("Enemy found at " + str getPos _enemyUnit);
 *   } else {
 *     sideChat "Everything is quiet around here.";
 *   }
 * ----------------------------------------------------------------------------------------------------
 */

// Get input variables
params[["_unit", objNull, [objNull]], // Get the unit
       ["_target", objNull, []], // Get the target
       ["_fov", 90, [90]], // Get the FoV
       ["_threshold", 0, [0]]]; // Get the threshold

// Perform parameter checks
// Check the unit
if(isNull _unit) exitWith {
  systemChat "isInLoS: Unit is null. Exiting..."; // Exit and inform
};

// Check the target
if(isNull _target) exitWith {
  systemChat "isInLoS: Target is null. Exiting..."; // Exit and inform
};

// Check the FoV
if(_fov <= 0 || {_fov >= 360}) then {
  _fov = 90; // Set to default
  systemChat "isInLoS: FoV must be in the range (0, 360) inclusive. Setting it to 90..."; // Inform of the changes
};

// Check the threshold
if(_threshold < 0 || {_threshold > 1}) then {
  _threshold = 0; // Set to default
  systemChat "isInLoS: Threshold must be in the range [0, 1] inclusive. Setting it to 0..."; // Inform of the changes
};

// Calculate vectors needed for calculations
private _lookDir = eyeDirection _unit; // Get the direction the unit looks at
private _objectAngle = (getPos _unit) vectorFromTo (getPos _target); // The direction of the target
_objectAngle = acos (_lookDir vectorDotProduct _objectAngle); // Find the angle between the direction the unit looks and the object

private _inLoS = false; // Initialize the inLoS flag
if(_objectAngle < _fov/2) then {
  _inLoS = [_unit, "VIEW", _target] checkVisibility [eyePos _unit, getPosASL _target] // Check for visibility
  if(_inLoS >= _threshold) then {
    _inLoS = true; // Set the inLoS flag
  };
};

// Return and exit
_inLoS;
