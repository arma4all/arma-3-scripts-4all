/* ----------------------------------------------------------------------------------------------------
 * Find if an object is in the Field-of-View of a unit. Only single objects are supported (no arrays).
 * The script does not account for obstacles obstructing the Line-of-Sight.
 * The scipt was inspired from a video by Feuerex.
 * ----------------------------------------------------------------------------------------------------
 * Author: Achilles.
 * ----------------------------------------------------------------------------------------------------
 * Input
 * ----------
 * unit [Object]: The unit whose FoV to be calculated.
 * target [Object]: What the unit looks for.
 * fov [Number] (Optional): Angle of FoV of the unit in degrees (defaults to 90 - i.e. ±45).
 * ----------------------------------------------------------------------------------------------------
 * Output
 * ----------
 * inFoV [Boolean]: Whether the object is in the field of view.
 * ----------------------------------------------------------------------------------------------------
 * How-To:
 * ----------
 * Call the script as a function.
 * ----------------------------------------------------------------------------------------------------
 * Example
 * ----------
 * Check whether an enemy unit is in the FoV of a friendly. If yes call for reinforcements.
 *
 *   _visible = [_friendlyUnit, _enemyUnit, 110] call compile preprocessFileLineNumbers "isInFoV.sqf";
 *   if(_visible) then {
 *     sideChat ("Enemy found at " + str getPos _enemyUnit);
 *   } else {
 *     sideChat "Everything is quiet around here.";
 *   }
 * ----------------------------------------------------------------------------------------------------
 */

// Get input variables
params[["_unit", objNull, [objNull]], // Get the unit
       ["_target", objNull, []], // Get the target
       ["_fov", 90, [90]]]; // Get the FoV

// Perform parameter checks
// Check the unit
if(isNull _unit) exitWith {
  systemChat "isInFoV: Unit is null. Exiting..."; // Exit and inform
};

// Check the target
if(isNull _target) exitWith {
  systemChat "isInFoV: Target is null. Exiting..."; // Exit and inform
};

// Check the FoV
if(_fov <= 0 || {_fov >= 360}) then {
  _fov = 90; // Set to default
  systemChat "isInFoV: FoV must be in the range (0, 360) inclusive. Setting it to 90..."; // Inform of the changes
};


// Calculate vectors needed for calculations
private _lookDir = eyeDirection _unit; // Get the direction the unit looks at
private _objectAngle = (getPos _unit) vectorFromTo (getPos _target); // The direction of the target
_objectAngle = acos (_lookDir vectorDotProduct _objectAngle); // Find the angle between the direction the unit looks and the object

private _inFoV = false; // Initialize the inFoV flag
if(_objectAngle < _fov/2) then {
  _inFoV = true; // Set the inFoV flag
};

// Return and exit
_inFoV;
