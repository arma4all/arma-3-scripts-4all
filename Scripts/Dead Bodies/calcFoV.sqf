/* ----------------------------------------------------------------------------------------------------
 * Calculate two unit vectors pointing in the directions of a unit's Field-of-View edges.
 * The scipt was inspired from a video by Feuerex.
 * ----------------------------------------------------------------------------------------------------
 * Author: Achilles
 * ----------------------------------------------------------------------------------------------------
 * Input
 * ----------
 * unit [Object]: The unit whose FoV to be calculated.
 * angle [Number] (Optional): Angle of FoV of the unit in degrees (defaults to 90 - i.e. ±45).
 * ----------------------------------------------------------------------------------------------------
 * Output
 * ----------
 * fov [Array - Array - Number]: Two 3D unit vectors pointing at the directions of the boundaries
 *                               of the FoV.
 *
 * fov select 0 select 0: Left fov edge directional vector's x-coordinate
 *              select 1: Left fov edge directional vector's y-coordinate
 *              select 2: Left fov edge directional vector's z-coordinate
 * fov select 1 select 0: Right fov edge directional vector's x-coordinate
 *              select 1: Right fov edge directional vector's y-coordinate
 *              select 2: Right fov edge directional vector's z-coordinate
 * ----------------------------------------------------------------------------------------------------
 * How-To:
 * ----------
 * Call the script as a function.
 * ----------------------------------------------------------------------------------------------------
 * Example
 * ----------
 * Calculate the vectors pointing in the direction of the edges of the Field-of-View (FoV) of a unit.
 * The FoV is set to 70 degrees (i.e. ±35 on each side).
 *
 *   _fovEdges = [_unit, 70] call compile preprocessFileLineNumbers "calcFoV.sqf";
 *
 * - _fovEdges: The two vectors pointing at the direction of the FoV edges
 * - _unit: The unit whose FoV edges we want to get
 * ----------------------------------------------------------------------------------------------------
 */

// Get input variables
params[["_unit", objNull, [objNull]], // Get the unit
       ["_angle", 90, [90]]]; // Get the angle

// Perform parameter checks
// Check the unit
if(isNull _unit) exitWith {
  systemChat "calcFoV: Unit is null. Exiting..."; // Exit and inform
};

// Check the angle
if(_angle <= 0 || {_angle >= 360}) then {
  _angle = 90; // Set to default
  systemChat "calcFov: Angle must be in the range (0, 360) inclusive. Setting it to 90..."; // Inform of the changes
};

// Calculate the directional vectors
private _lookDir = eyeDirection _unit; // The direction the unit is looking

// Calculate geometrical variables
_angleSin = sin(_angle/2); // Sin of angle
_angleCos = cos(_angle/2); // Cosine of angle

// Rotate the reference vector (where the unit looks)
private _fovEdge = [((_lookDir select 0) * _angleCos) - ((_lookDir select 1) * _angleSin), // Calculate the x-coordinate
                    ((_lookDir select 0) * _angleSin) + ((_lookDir select 1) * _angleCos), // Calculate the y-coordinate
                    _lookDir select 2]; // Z-coordinate stays the same

_fovEdge = _fovEdge pushBack _fovEdge; // Copy the calculated vector
(_fovEdge select 1) set[1, -((_fovEdge select 1) select 1)]; // Reverse the y-coordinate of the second edge/boundary

// Return and exit
_fovEdge;
