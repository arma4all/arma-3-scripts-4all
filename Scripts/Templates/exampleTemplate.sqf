/* What follows is an example of a script.
 * It is presented just to demonstrate the use of the template.
 */

/* ----------------------------------------------------------------------------------------------------
 * Script to append a specific amount of null objects to an array and return.
 * The returned array is a (deep) copy of the first with the addition of the specified
 * amount of null objects appended at the end.
 * If the array does not exist (is nil) or is empty, an array with only null objects (as many as
 * specified) is returned.
 * ----------------------------------------------------------------------------------------------------
 * Author: 0bl!v!usNuLL!s7
 * ----------------------------------------------------------------------------------------------------
 * Input
 * ----------
 * array [Any]: Original array
 * numOfElems [Number]: Number of elements to add (defaults to 0)
 * ----------------------------------------------------------------------------------------------------
 * Output
 * ----------
 * newArray: The filled array
 * ----------------------------------------------------------------------------------------------------
 * How-To:
 * ----------
 * Execute the script whereever you need to.
 * ----------------------------------------------------------------------------------------------------
 * Example
 * ----------
 * _newArray = [_oldArray, 13] execVM "appendNulls.sqf";
 * ----------------------------------------------------------------------------------------------------
 */

// Get variables
params[["_originalArray", [], [[]]], // The original array
       ["_numberOfElementsToAdd", 0, [0]]]; // How many elements to add

// Perform parameter checks
// Number of elements
if(_numberOfElementsToAdd isEqualTo 0) exitWith {
  _numberOfElementsToAdd = -1; // Set -1 to skip the for loop
  systemChat "exampleTemplate: _numberOfElementsToAdd is zero. Returning the original array"
  _originalArray;
};

// Original Array
private _newArray = [];
if(count _originalArray isEqualTo 0 || {_originalArray isEqualTo nil}) then {
  if(_numberOfElementsToAdd isEqualTo -1) exitWith {};
  _newArray = [objNull]; // Initialize array
  _numberOfElementsToAdd = _numberOfElementsToAdd - 1; // Subtract on element to add (added in initialization above)
} else {
  // Copy the array
  _newArray = +_originalArray; // Make a deep copy of the original array
};

// Append elements
for[{private _i = 0}, {_i < _numberOfElementsToAdd}, {_i = _i + 1}] do {
    _newArray pushBack objNull; // Append a null object to the array
};

// Return and exit
_newArray; // Return the filled array
