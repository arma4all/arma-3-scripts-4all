/* ----------------------------------------------------------------------------------------------------
 * This functions calculates day and night durations multipliers that result in certain durations
 * Two multipliers are returned, one for the day duration and one for the night. Additionally, the
 * day and night times returned, which are calculated based on the susrise and sunset of the given
 * ArmA day.
 * ----------------------------------------------------------------------------------------------------
 * Author: Achilles
 * Date: 23/04/2020 (DD/MM/YYYY)
 * ----------------------------------------------------------------------------------------------------
 * Input
 * ----------
 * dayDur [Number] (Optional): Duration of the whole day in real hours (defaults to 24)
 * dnFac [Number] (Optional): The day-to-night ratio (defaults to 1)
 * dayOfYear [date] (Optional): The day of the year for which the multipliers will be
 *                              calculated (defaults to the current ArmA date)
 * check [Boolean] (Optional): Perform parameter checks flag (defaults to true)
 * ----------------------------------------------------------------------------------------------------
 * Output
 * ----------
 * data [Array]: - _this select 0: Day multiplier
 *               - _this select 1: Night multiplier
 *               - _this select 2: Sunrise time
 *               - _this select 3: Sunset time
 *
 * ----------------------------------------------------------------------------------------------------
 * How-To:
 * ----------
 * Call the function with the desired total day duration (in real-life hours), the day-night ratio and
 * desired date in ArmA 3 date format
 * (this is [year, month (1-12), day (1-31), hour (0 - 23), minute (0-59)]).
 *
 * ----------------------------------------------------------------------------------------------------
 * Example
 * ----------
 * This example makes use of all the returned parameters to set the time multipliers for the day and
 * night periods.
 *
 * _wholeDay = 3; // Whole day will last 3 hours in real life
 * _dayNightFrac = 2; // Day is twice as long as the night
 * _myDate = date; // Use the current ArmA 3 date
 *
 * _timeData = [_wholeDay, _dayNightFrac, _myDate] call A4A_fnc_timeMuls; // Get the time multipliers
 *                                                                        // and rise and set times
 *
 * // Manage time
 * while{true} do {
 *  if(daytime > (_timeData select 3) || {daytime < (_timeData select 2)}) then {
 *    setTimeMultiplier (_timeData select 1); // Use night time multiplier
 *  } else {
 *    setTimeMultiplier (_timeData select 0); // Use day time multiplier
 *  };
 * };
 * ----------------------------------------------------------------------------------------------------
 */

// Get input variables
params[["_dayDur", 24, [24]], // Total duration of day
       ["_dnFac", 1, [1]], // Day-to-Night factor
       ["_dayOfYear", date, [date]], // Asked date
       ["_check", true, [true]]]; // Parameter checks flag

// Check if we are to perform checks
if(_check) then {
  // Perform parameter checks
  // Check day duration
  if(_dayDur <= 0) exitWith {
    ["Day duration must be non-negative."] call BIS_fnc_error; // Throw an error
    [nil, nil, nil, nil] // Return nil filled array
  };

  // Check day-to-night factor
  if(_dnFac) exitWith {
    ["Day-to-Night ratio must be non-negative."] call BIS_fnc_error;
    [nil, nil, nil, nil] // Return nil filled array
  };

  // Check specified date one element at a time (for clarity)
  // Variable to use for testing
  private _test = _dayOfYear select 0; // Take year

  // Check year
  if(isNull _test || {_test <= 0}) then {
    ["Invalid date format."] call BIS_fnc_error; // Throw error
    [nil, nil, nil, nil]; // Return nil filled array
  };

  // Check month
  _test = _dayOfYear select 1; // Take month

  if(isNull _test || {_test < 1} || {_test > 12}) then {
    ["Invalid date format."] call BIS_fnc_error; // Throw error
    [nil, nil, nil, nil]; // Return nil filled array
  };

  // Check day
  _test = _dayOfYear select 2; // Take day

  if(isNull _test || {_test < 1} || {_test > 31}) then {
    ["Invalid date format."] call BIS_fnc_error; // Throw error
    [nil, nil, nil, nil]; // Return nil filled array
  };

  // Check hour
  _test = _dayOfYear select 3; // Take hour

  if(isNull _test || {_test < 0} || {_test > 23}) then {
    ["Invalid date format."] call BIS_fnc_error; // Throw error
    [nil, nil, nil, nil]; // Return nil filled array
  };

  // Check minute
  _test = _dayOfYear select 4; // Take minute

  if(isNull _test || {_test < 0} || {_test > 59}) then {
    ["Invalid date format."] call BIS_fnc_error; // Throw error
    [nil, nil, nil, nil]; // Return nil filled array
  };
};

// Main body of the function
// Declare some variables
private _riseSet = _dayOfYear call BIS_fnc_sunriseSunsetTime; // Get the sunrise and sunset times of the day
private _durs = [nil, nil];

// Calculate day and night duration (in real life hours)
_durs set[0, (_riseSet select 1) - (_riseSet select 0)]; // Calculate day duration
_durs set[1, 24 - (_durs select 0)]; // Calculate night duration

// Calculate multipliers
/* Solve simultaneously:
 *
 * dayDur * dayMul + nightDur * nightMul = 24 (1)
 * (dayDur * dayMul)/(nightDur * nightMul) = dayNightFrac (2)
 *
 * The result is:
 *
 * dayMul = 24/(nightDur * (1 + dayNightFrac))
 * nightMul = (nightDur * dayNightFrac * dayMul)/dayDur
 */
private _dMul = 24/((_durs select 1) * (1 + _dnFac)); // Calculate the day multiplier
private _nMul = ((_durs select 1) * _dnFac * _dMul)/(_durs select 0); // Calculate the night multiplier

// Multiply with "global multiplier"
_dMul = _dMul * 24/_dayDur; // Final day multiplier
_nMul = _nMul * 24/_dayDur; // Final night multiplier

// Return and exit
[_dMul, _nMul, _riseSet select 0, _riseSet select 1]
