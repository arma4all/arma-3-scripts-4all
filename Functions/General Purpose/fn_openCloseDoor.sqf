/* ----------------------------------------------------------------------------------------------------
 * Function to open and close a door.
 * ----------------------------------------------------------------------------------------------------
 * Author: Achilles
 * ----------------------------------------------------------------------------------------------------
 * Input
 * ----------
 * building [Object]: The building
 * door [Number] (Optional): The number representing the door (defaults to 1)
 * action [Number] (Optional): Number representing the desired action. 1 to open and
 *                             0 to close (defaults to 0)
 * ----------------------------------------------------------------------------------------------------
 * Output
 * ----------
 * Nothing
 * ----------------------------------------------------------------------------------------------------
 * How-To:
 * ----------
 * - Call the function.
 * ----------------------------------------------------------------------------------------------------
 * Example
 * ----------
 * - Place the function in two addAction commands of a tablet to be used to open/close a door.
 *
 *    _tablet addAction["Open Door", {_building = _this select 3; [_building, 2, 1] call A4A_fnc_openCloseDoor}, _building];
 *    _tablet addAction["Close Door", {_building = _this select 3; [_building, 2, 0] call A4A_fnc_openCloseDoor}, _building];
 *
 * - _tablet: The tablet used to open and close the building's door
 * - _building: The building/hangar with the door
 * ----------------------------------------------------------------------------------------------------
 */

// Get input variables
params[["_building", objNull, [objNull]], // The building
       ["_door", 1, [1]], // The number representing the door
       ["_action", 0, [0]]]; // The desired action (0 -> close, 1 -> open)

// Perform parameter checks
// Check building
if(isNull _building) exitWith {
  systemChat "openCloseDoor: _building is objNull. Exiting..."; // Exit and inform of the error
};

// Check door
private _numOfDoors = getNumber (configFile >> "CfgVehicles" >> (typeOf _building) >> "numberOfDoors"); // Get the number of doors the building has
if(_numOfDoors isEqualTo -1 || {_numOfDoors isEqualTo 0}) exitWith {
  systemChat "openCloseDoor: Building has no doors. Exiting...";
};

_door = floor _door; // Make sure door is an integer

// Check door number is a valid one
if(_door < 1) then {
  _door = 1; // Set to default
  systemChat "openCloseDoor: _door is less than one. Setting to one..."; // Inform of the changes
} else {
  // Check availability of door
  if(_door > _numOfDoors) then {
    _door = 1; // Set default
    systemChat format["openCloseDoor: _door is greater than the available doors (%1). Setting to one...", _numOfDoors]; // Inform of the changes
  };
};

// Check action
_action = floor _action; // Make sure it's an integer

if(_action != 0 && {_action != 1}) then {
  _action = "Close"; // Set to default
  systemChat "openCloseDoor: Wrong _action number. Setting to zero (close door)..."; // Inform of the changes
} else {
  // Make action a string to use later on
  if(_action isEqualTo 0) then {
    _action = "Close";
  } else {
    _action = "Open";
  };
};

// Find the appropriate code in the configFile
private _configPath = configFile >> "CfgVehicles" >> (typeOf _building) >> "UserActions"; // Hold the path up to this point in a variable
private _allDoors = "true" configClasses (_configPath); // Get the UserAction classes for this type of building

//_allDoors apply{configName _x}; // Get the names of the classes as strings in an array

// Look for a string containing 1) the word "door", 2) the requested number, 3) the requested action
_door = _allDoors findIf {
  (((configName _x) find "Door") != -1) && {((configName _x) find (str _door)) != -1} && {((configName _x) find _action) != -1}
};

// Execute the action on the door
_door = getText (_configPath >> configName (_allDoors select _door) >> "statement"); //Get the statement to be executed
_door = _door splitString "this"; // Get rid of the "this" variable (which is undefined in the current scope)

// Check if "this" was between other characters
if((count _door) > 1) then {
  _door = format["%1%2%3", (_door select 0), (str _building), (_door select 1)]; // Place the name of the building where "this" was before
} else {
  _door = format["%1%2", (str _building), (_door select 0)]; // Place the name of the building where "this" was before
};

// Execute the statement to perform the action
call (compile _door);
