/* ----------------------------------------------------------------------------------------------------
 * Script to empty a container.
 * ----------------------------------------------------------------------------------------------------
 * Author: Achilles
 * ----------------------------------------------------------------------------------------------------
 * Input
 * ----------
 * container [Object]: The container to be emptied
 * ----------------------------------------------------------------------------------------------------
 * Output
 * ----------
 * Nothing
 * ----------------------------------------------------------------------------------------------------
 * How-To:
 * ----------
 * - Call the script.
 * ----------------------------------------------------------------------------------------------------
 * Example
 * ----------
 * - Place the function in an addAction command of a litter bin to be used to empty it.
 *
 *    _litterBin addAction["Empty", {_theBin = _this select 0; _theBin call A4A_fnc_emptyContainer}];
 *
 * - _litterBin: The bin the action is attached to
 * ----------------------------------------------------------------------------------------------------
 */

// Get variables
params[["_container", objNull, [objNull]]];

// Perform parameter checks
if(_bin isEqualTo objNull) exitWith {
  systemChat "emptyContainer: _container is objNull. Exiting..."; // Exit and inform of the error
};

// Clear the contents of the bin
clearItemCargoGlobal _container; // Delete the items
clearWeaponCargoGlobal _container; // Delete the weapons
clearMagazineCargoGlobal _container; // Delete the magazines
clearBackpackCargoGlobal _container; // Delete the backpacks
