// Create the class holding the functions information
class cfgFunctions {
  // Tag
  class A4A {
    tag = "A4A"; // The functions' tag

    class functions {
      file = "General Purpose"; // The file holding the functions

      // The functions
      class emptyContainer {
        Description = "Function to empty a container"; // Description of the function
      };

      class openCloseDoor {
        Description = "Function to open and close doors"; // Description of the function
      };

      class timeMuls {
        Description = "Function to manage the day and night durations"; // Description of the function
      };
    };
  };
};
