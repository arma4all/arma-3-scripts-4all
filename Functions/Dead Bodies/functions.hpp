// Create the class holding the functions information
class cfgFunctions {
  // Tag
  class A4A {
    tag = "A4A"; // The functions' tag

    class functions {
      file = "Dead Bodies"; // The file holding the functions

      // The functions
      class bodiesInFoV {
        Description = "Function to check if a unit is in the Field-of-View of another"; // Description of the function
      };

      class bodiesInFoVInternal {
        Description = "Function to check if a unit is in the Field-of-View of another"; // Description of the function
      };

      class calcFoV {
        Description = "Function to calculate the two vectors pointing in the direction of the borders of the Field-of-View"; // Description of the function
      };

      class isInFoV {
        Description = "Function to check if an object is in the Field-of-View of a unit"; // Description of the function
      };

      class isInLoS {
        Description = "Function to check if an object is in the Line-of-Sight of a unit"; // Description of the function
      };

      class isInLoSInternal {
        Description = "Function to check if an object is in the Line-of-Sight of a unit"; // Description of the function
      }
    };
  };
};
