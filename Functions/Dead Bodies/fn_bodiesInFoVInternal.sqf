/* ----------------------------------------------------------------------------------------------------
 * Calculate the dead bodies in the Field-of-View of a unit up to a maximum distance. The function
 * does NOT account for objects obstructing line of sight.
 * The function uses "fn_isInFoV" internally so this is a dependency.
 * The function was inspired from a video by Feuerex.
 * ----------------------------------------------------------------------------------------------------
 * Author: Achilles.
 * ----------------------------------------------------------------------------------------------------
 * Input
 * ----------
 * unit [Object]: The unit whose FoV to be calculated.
 * fov [Number] (Optional): Angle of FoV of the unit in degrees (defaults to 90 - i.e. ±45).
 * distance [Number] (Optional): Distance to look for dead bodies in metres (defaults to 10).
 * ----------------------------------------------------------------------------------------------------
 * Output
 * ----------
 * visibleBodies [Array - Objects]: The dead units in the FoV.
 * ----------------------------------------------------------------------------------------------------
 * How-To:
 * ----------
 * Call the function.
 * ----------------------------------------------------------------------------------------------------
 * Example
 * ----------
 * Use this to calculate the dead bodies in a unit's field-of-view. The function returns an array
 * with all the dead bodies in the field of view of the unit.
 *
 *   _visibleBodies = [_unit, 80, 35] call A4A_fnc_bodiesInFoVInternal;
 *   if(count _visibleBodies > 0) {
 *     hint "Dead body found!!!";
 *   } else {
 *     hint "Everything is quite around here...";
 *   }
 *
 * - _visibleBodies: The array to hold the dead bodies
 * - _unit: The unit searching for things
 * ----------------------------------------------------------------------------------------------------
 */

// Get input variables
params[["_unit", objNull, [objNull]], // Get the unit
      ["_fov", 90, [90]] // Get the FoV
      ["_distance", 10, [10]]]; // Get the distance

// Perform parameter checks
// Check the unit
if(isNull _unit) exitWith {
  systemChat "bodiesInFoVInternal: Unit is null. Exiting..."; // Exit and inform
};

// Check the FoV
if(_fov <= 0 || {_fov >= 360}) then {
  _fov = 90; // Set to default
  systemChat "bodiesInFoVInternal: FoV must be in the range (0, 360) inclusive. Setting it to 90..."; // Inform of the changes
};

// Check the distance
if(_distance <= 0) then {
  distance = 10; // Set to default
  systemChat "bodiesInFoVInternal: Distance must be greater than zero. Setting it to 10..."; // Inform of the changes
};

// Find close dead bodies
private _deadBodies = []; // Initialize variable to hold dead bodies close to the unit
{
  // Check the distance between the unit and dead body
  if(_x distance _unit <= _distance) then {
    _deadBodies pushBack _x; // Add the dead body to the array
  };
} forEach allDead;

// See which of the dead bodies are in the FoV
private _visibleBodies = []; // Initialize variable to hold visible dead bodies
{
  _inFoV = [_unit, _x, _fov] call A4A_fnc_isInFoV; // Check if the body is in the FoV
  if(_inFoV) then {
    _visibleBodies pushBack _x; // Add the body to the array
  };
} forEach _deadBodies;

// Return and exit
_visibleBodies;
