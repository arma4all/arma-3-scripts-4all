/* ----------------------------------------------------------------------------------------------------
 * Script to add/remove action on people entering/leaving an area.
 * The script is meant to be used inside the onActivation field of a trigger.
 * The script performs checks every 10 seconds. This can be changed with an optional parameter
 * ----------------------------------------------------------------------------------------------------
 * Author: Achilles
 * ----------------------------------------------------------------------------------------------------
 * Input
 * ----------
 * trigger [Object]: The trigger the script is used in
 * actionName [String]: The name of the action to be added
 * scriptPath [String]: The path to the script "link" to the action
 * checkInterval [Number] (Optional): How often will the script check for players inside
 *                                    the trigger area (defaults to 10)
 * arguments [Any] (Optional): Additional arguments to the script (defaults to nil)
 * priority [Number] (Optional): Action's priority (defaults to 1.5)
 * showWindow [Boolean] (Optional): Whether TitleText is shown at mid-low screen (defaults to true)
 * hideOnUse [Boolean] (Optional): Hide action after use (defaults to true)
 * shortcut [String] (Optional): A key name from ArmA's available ones (defaults to "")
 * condition [String] (Optional): Script code that must evaluate to Boolean. For more info see
 *                                addAction's documentation (defaults to "true")
 * radius [Number] (Optional): 3D distance (in meters) where the action is visible (defaults to 50)
 * unconscious [Boolean] (Optional): Whether the action will be show to incapacitated players
 *                                   (defaults to false)
 * selection [String] (Optional): Named selection in Geometry LOD to which the action is attached
 *                                (defaults to "")
 * memoryPoint [String] (Optional): Memory point to which the action is attached. For more info see
 *                                  addAction's documentation (defaults to "")
 *
 * ----------------------------------------------------------------------------------------------------
 * Output
 * ----------
 * Nothing
 * ----------------------------------------------------------------------------------------------------
 * How-To
 * ----------
 * - Place following command in the onActivation field of a trigger:
 *
 *     [thisTrigger, "actionName", "scriptPath"] call A4A_fnc_addRemoveAction;
 *
 * - thisTrigger: This is the trigger object passed to the onActivation codeblock of the trigger.
 * - "actionName": The name of the action to be added
 * - "scriptPath": The (relative) path to the script to be run (script name included)
 * ----------------------------------------------------------------------------------------------------
 * Example
 * ----------
 * - When a player goes close to a specified medical building (in the trigger area) add a "Full heal"
 *   action to allow them to get completely healed. In the onActivation field of the trigger add:
 *
 *    [thisTrigger, "Heal", "MyScripts\Medical\fullHeal.sqf", 10] call A4A_fnc_addRemoveAction];
 * ----------------------------------------------------------------------------------------------------
 */

// Get variables
params[["_trigger", objNull, [objNull]], // The trigger
       ["_actionName", "defaultAction", [""]], // The name of the action
       ["_scriptPath", "", [""]], // The path to the script
       ["_checkInterval", 10, [10]], // Check interval
       [_"arguments", nil, []], // Additional arguments to the script
       ["_priority", 1.5, [1.5]], // Priority of action
       ["_showWindow", true, [true]], // showWindow for action
       ["_hideOnUse", true, [true]], // hideOnUse for action
       ["_shortcut", "", [""]], // Shortcut of action
       ["_condition", "true", [""]], // Condition for action to happen
       ["_radius", 50, [50]], // Action activation radius
       ["_unconscious", false, [false]], // Unconscious for action
       ["_selection", "", [""]], // Selection for action
       ["_memoryPoint", "", [""]]]; // memoryPoint for action

/*
 * Perform memory checks
 */
// Trigger
if(_trigger isEqualTo objNull) exitWith {
  systemChat "addRemoveAction: _trigger is objNull. Exiting...";
};

// Action name
// Nothing to check

// Script path
if(_scriptPath isEqualTo "") exitWith {
  systemChat "addRemoveAction: Wrong script or no script path specified. Exiting...";
};

// Check interval
if(_checkInterval <= 0) then {
  _checkInterval = 10; // Set to default
  systemChat "addRemoveAction: Check interval less or equal to zero. Setting to default (10)..."; // Inform of the changes
};

// Arguments
// Nothing to check

// Priority
if(_priority <= 0) then {
  _priority = 1.5; // Set to default
  systemChat "addRemoveAction: _priority is negative or zero. Setting to default (1.5)..."; // Inform of changes
};

// showWindow
// Nothing to check

// hideOnUse
// Nothing to check

// Shortcut
// Nothing to check

// Condition
// Nothing to check

// Radius
if(_radius <= 0) then {
  _radius = 50; // Set to default
  systemChat "addRemoveAction: _radius is negative or zero. Setting to default (50)..."; // Inform of the changes
};

// Unconscious
// Nothing to check

// Selection
// Nothing to check

// memoryPoint
// Nothing to check

/*
 * End of memory checks
 */

// Look for players inside triggers region
private _previousPlayers = [; // Initialize array to hold the players previously inside the area
private _actionIds = []; // Initialize array to hold the IDs of the actions

while{true} do {
  _playersInArea = allPlayers - entities "HeadlessClient_F"; // Get all the players without headless clients
  _playersInArea = _playersInArea select{alive _x}; // Get the alive players
  _playersInArea = _playersInArea inAreaArray _trigger; // Find players inside the area of the trigger

  // Go through the players that were here last time
  {
    // Check if player left the area
    if(!(_x in _playersInArea)) then {
      _x removeAction (_actionIds select _forEachIndex); // Remove action from the player
    };
  } forEach _previousPlayers;

  // Check if there are players left in the area
  if(count _playersInArea <= 0) exitWith {}; // When there are no players inside the area exit the loop

  // Go through the players in the area
  {
    // Check if this is a new player in the area
    if(!(_x in (_previousPlayers select 0))) then {
      // Add action
      _iD = _x addAction [_actionName, // Action name
                     _scriptPath, // Script to execute
                     , // The trigger as an argument (see script for more info)
                     _priority, // Priority
                     _showWindow, // Whether it will be shown as text at mid-low screen
                     _hideOnUse, // hideOnUse
                     _shortcut, // Shortcut
                     _condition, // Condition
                     _radius, // Radius
                     _unconscious, // Unconscious
                     _selection, // Selection
                     _memoryPoint]; // Memory Point

      _actionIds set [_forEachIndex, _iD]; // Add the action ID to the array
    } else {
      _currentPlayer = _playersInArea select _forEachIndex; // Find the player we are currently looking at
      _idIndex = _previousPlayers findIf{_x isEqualTo _currentPlayer}; // Find the index of the player we are looking at in the previous players array
      _currentAction = _actionIds select _idIndex; // Get the ID of the player's action
      _actionIds set [_forEachIndex, _currentAction]; // Place the action ID at the same index as the player in the current players in the area array
    };
  } forEach _playersInArea;

  _actionIds resize count _playersInArea; // Resize the array to get rid of "unused" elements
  _previousPlayers = +_playersInArea; // Hold the players currently in the area (deep copy)

  sleep _checkInterval; // Sleep for 5 seconds before checking again for people
};
