// Create the class holding the functions information
class cfgFunctions {
  // Tag
  class A4A {
    tag = "A4A"; // The functions' tag

    class functions {
      file = "Trigger-Used"; // The file holding the functions

      // The functions
      class addRemoveAction {
        Description = "Function to add and remove actions"; // Description of the function
      };
    };
  };
};
