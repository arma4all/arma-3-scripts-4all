// Create the class holding the functions information
class cfgFunctions {
  // Tag
  class A4A {
    tag = "A4A"; // The functions' tag

    class functions {
      file = "Templates"; // The file holding the functions

      // The functions
      class exampleTemplate {
        Description = "An example template of a function"; // Description of the function
      };

      class functionTemplate {
        Description = "An empty function serving as a template to start coding"; // Description of the function
      };
    };
  };
};
