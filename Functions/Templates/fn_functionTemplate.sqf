/* ----------------------------------------------------------------------------------------------------
 * Function Template <-- Here place the description of your function (what it does)
 * ----------------------------------------------------------------------------------------------------
 * Author: Your name <-- Place the name you want to be referenced by (if you don't want to add a name
 *                       either write unknown or leave blank and will be filled by the maintainers)
 * ----------------------------------------------------------------------------------------------------
 * Input
 * ----------
 * Variables: Description <-- Here place the variables your function excpect as inputs
 * ----------------------------------------------------------------------------------------------------
 * Output
 * ----------
 * Variable: Description <-- Here place the variable your function returns as output
 * ----------------------------------------------------------------------------------------------------
 * How-To:
 * ----------
 * Instructions <-- Here place the instructions of the function's usage
 * ----------------------------------------------------------------------------------------------------
 * Example
 * ----------
 * Here you can place an example. It is optional but highly recommended to help other people understand
 * better how to use your function.
 * ----------------------------------------------------------------------------------------------------
 */

// Get input variables


// Perform parameter checks


// Main body of the function


// Return and exit
