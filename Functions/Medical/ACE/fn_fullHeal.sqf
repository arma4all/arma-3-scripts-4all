/* ----------------------------------------------------------------------------------------------------
 * Function to fully heal a unit.
 * Heals all wounds of a unit. Adapted from ACE 3
 * link: https://github.com/Pergor/ADV_MissionTemplate/blob/master/adv_missiontemplate.altis/functions/client/fn_fullHeal.sqf
 * ----------------------------------------------------------------------------------------------------
 * Author: Achilles
 * ----------------------------------------------------------------------------------------------------
 * Input
 * ----------
 * unit [Object]: The unit to heal
 * ----------------------------------------------------------------------------------------------------
 * Output
 * ----------
 * Nothing
 * ----------------------------------------------------------------------------------------------------
 * How-To:
 * ----------
 * - Call the function.
 * ----------------------------------------------------------------------------------------------------
 * Example
 * ----------
 * - Place the function in the "onActivation" field of a trigger to fully heal the units in the
 *   trigger area.
 *
 *    {
 *      _x call A4A_fnc_fullHeal;
 *    } forEach thisList;
 *
 * - thisList: A list with the units inside the trigger area (for more info see trigger documentation)
 * - _x: Magic variable of "forEach" construct (for more info see documentation of forEach)
 * ----------------------------------------------------------------------------------------------------
 */

// Get variables
params[["_unit", objNull, [objNull]]];

// Perform parameter checks
if(_unit isEqualTo objNull) exitWith {
  systemChat "fullHeal: _unit is objNull. Exiting..."; // Exit and inform of the problem
};

// Heal the unit
[objNull, _unit] call ace_medical_fnc_treatmentAdvanced_fullHealLocal; // Fully heal unit
