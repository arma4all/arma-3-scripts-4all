/* ----------------------------------------------------------------------------------------------------
 * Script to apply wounds to a player.
 * ----------------------------------------------------------------------------------------------------
 * Author: Achilles
 * ----------------------------------------------------------------------------------------------------
 * Input
 * ----------
 * wounded [Object]: The unit to wound
 * numOfWounds [Number] (Optional): How many wounds to inflict (defaults to 1)
 * severity [Number] (Optional): How serious the wounds will be. Must be a number greater than
 *                               0 and up 1 (defaults to 0.5)
 * ----------------------------------------------------------------------------------------------------
 * Output
 * ----------
 * Nothing
 * ----------------------------------------------------------------------------------------------------
 * How-To
 * ----------
 * Execute the script.
 * ----------------------------------------------------------------------------------------------------
 * Example
 * ----------
 * - Place the function in the "onActivation" field of a trigger to wound a unit that enters the
 *   trigger area.
 *
 *    _unitToWound = thisList select 0; // Select the first unit in the area
 *    [_unitToWound] call A4A_fnc_inflictWounds; // Inflict wounds
 *
 *    // Wound the second unit and choose the amount of wounds and the severity
 *    _unitToWound = thisList select 1; // Select the second unit (assume it exists)
 *    [_unitToWound, 3, 0.8] call A4A_fnc_inflictWounds;
 *
 * - thisList: A list with the units inside the trigger area (for more info see trigger documentation)
 * ----------------------------------------------------------------------------------------------------
 */

// Get variables
params[["_wounded", objNull, [objNull]], // The unit to wound
       ["_numOfWounds", 1, [1]], // Number of wounds
       ["_severity", 0.5, [0.5]]]; // Severity of wounds

// Perform parameter checks
// Wounded unit
if(_wounded isEqualTo objNull) exitWith {
  systemChat "inflictWounds: _wounded is objNull. Exiting..."; // Exit and inform of the problem
};

// Number of wounds
_numOfWounds = floor _numOfWounds; // Make sure number of wounds is integer
if(_numOfWounds <= 1) then {
  _numOfWounds = 1; // Set to default
  systemChat "inflictWounds: _numOfWounds is less than one. Setting to one..."; // Inform of the changes
} else {
  _numOfWounds = floor _numOfWounds; // Make sure it is integer
};

// Severity
if(_severity <= 0 || {_severity > 1}) then {
  _severity = 0.5; // Set to default
  systemChat "inflictWounds: _severity is out of bounds. Setting to 0.5..."; // Inform of the changes
};

// Inflict the wounds
// Initialize variables necessary for "inflicting wounds"
private _bodyParts = ["head", "body", "hand_l", "hand_r", "leg_l", "leg_r"]; // All the possible body parts
private _woundTypes = ["bullet", "grenade", "explosive", "shell", "vehiclecrash", "backblast", "stab", "punch", "falling", "unknown"]; // All the possible wound types

for[{private _i = 0}, {_i < _numOfWounds}, {_i = _i + 1}] do {
  // Damage of wound is: random _severity + 0.01
  // Body part to be wounded is: selectRandom _bodyParts;
  // Type of wound to inflict is: selectRandom _woundTypes;
  [_wounded, (random _severity) + 0.01, selectRandom _bodyParts, selectRandom _woundTypes] call ace_medical_fnc_addDamageToUnit; // Inflict the wound
};
