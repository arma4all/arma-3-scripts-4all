// Create the class holding the functions information
class cfgFunctions {
  // Tag
  class A4A {
    tag = "A4A"; // The functions' tag

    class functions {
      file = "Medical\\ACE"; // The file holding the functions

      // The functions
      class fullHeal {
        Description = "Function to fully heal a unit"; // Description of the function
      };

      class inflictWounds {
        Description = "Function to inflict random wounds to a unit"; // Description of the function
      };
    };
  };
};
