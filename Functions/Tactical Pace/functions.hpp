// Create the class holding the functions information
class cfgFunctions {
  // Tag
  class A4A {
    tag = "A4A"; // The functions' tag

    class functions {
      file = "Tactical Pace"; // The file holding the functions

      // The functions
      class tacticalPace {
        Description = "Function to add tactical pace to a unit"; // Description of the function
      };
    };
  };
};
