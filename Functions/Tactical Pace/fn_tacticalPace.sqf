/* ----------------------------------------------------------------------------------------------------
 * Function to increase the speed of the walking pace of the players to use as an extra pace,
 * ----------------------------------------------------------------------------------------------------
 * Author: Achilles Kappis
 * ----------------------------------------------------------------------------------------------------
 * Input
 * ----------
 * unit [Object]: The unit whose pace you want to increase.
 * multiplier [Number]: This is the amount of the final speed. Normal speed is 1 with half being 0.5 and double
 *             being 2. Set this only if you are not satisfied with the preset implementation
 *             (defaults to 1.3).
 * UNUSED - RESERVED FOR FUTURE USE: staminaFactor [Number]: This is a factor affecting the fatigue of the unit while in the tactical pace.
 *                         The number ranges from 1 which is zero effect on stamina and is unbounded from above.
 *                         The greater the number, the greater the effect on stamina (defaults to 1.1).
 * swayInc [Number]: The sway increase due to the increased pace (defaults to 2).
 * recoilInc [Number]: The recoil increase due to the increased pace (defaults to 1).
 * ----------------------------------------------------------------------------------------------------
 * Output
 * ----------
 * Nothing.
 * ----------------------------------------------------------------------------------------------------
 * How-To:
 * ----------
 * You can call it however you see fit as long as you pass the appropriate variables.
 * ----------------------------------------------------------------------------------------------------
 * Example
 * ----------
 * Add an event handler for user-defined key 3 (on de-press - key up) to toggle the tactical pace of
 * a unit. The script increases the pace by 75% and adds 2 to the weapon sway and 1.5 to the recoil.
 *
 *   (findDisplay 46) displayAddEventHandler ["onKeyUp", {
 *       if((_this select 1) in actionKeys "User3") {
 *           [_theUnit, 1.75, 2, 1.5] call A4A_fnc_tacticalPace;
 *       };
 *   }];
 * ----------------------------------------------------------------------------------------------------
 */

// Get input variables
params[["_unit", objNull, [objNull]], // Get the unit
      ["_multiplier", 1.3, [1.3]], // Get the multiplier
      ["_staminaFactor", 1.1, [1.1]], // Get the stamina factor
      ["_swayInc", 2, [2]], // Get the weapon sway increase
      ["_recoilInc", 1, [1]]]; // Get the weapon recoil increase

// Perform parameter checks
// Check unit
if(isNull _unit) exitWith {
  systemChat "tacticalPace: Unit is null. Exiting..."; // Inform and exit
};

// Check multiplier
if(_multiplier <= 1) then {
  _multiplier = 1.3; // Set to default
  systemChat "tacticalPace: Multiplier must be greater than 1. Setting to 1.3..."; // Inform of the changes
};

// Check stamina factor
if(_staminaFactor < 1) then {
  _staminaFactor = 1.1; // Set to default
  systemChat "tacticalPace: Stamina factor must be greater or equal to 1. Setting to 1.1..."; // Inform of the changes
};

// Check sway increase
if(_swayInc < 0) then {
  _swayInc = 2; // Set to default
  systemChat "tacticalPace: Sway increase must be greater than 0. Setting to 2..."; // Inform of the changes
};

// Check recoil increase
if(_recoilInc < 0) then {
  _recoilInc = 1; // Set to default
  systemChat "tacticalPace: Recoil increase must be greater than 0. Setting to 1..."; // Inform of the changes
};

// Main body of the script
scopeName "doChecks"; // Create a scope to exit without waiting if conditions are not met

// Get current values of various parameters
private _unitSpeed = speed _unit; // Get the speed of the unit
private _animCoef = getAnimSpeedCoef _unit; // Get the animation speed coefficient
private _swayCoef = getCustomAimCoef _unit; // Get the aiming coefficient
private _recoilCoef = unitRecoilCoefficient _unit; // Get the recoil coefficient

// Check speed, if weapon is lowered and that the unit is not already in tactical pace
if(_unitSpeed < 6 && {!weaponLowered _unit} && {_animCoef <= 1}) then {
  _unit setAnimSpeedCoef (_animCoef * _multiplier); // Set the unit in tactical pace
  _unit setCustomAimCoef (_swayCoef + _swayInc); // Increase the sway
  _unit setUnitRecoilCoefficient (_recoilCoef + _recoilInc); // Increase the recoil
} else {
  breakOut "doChecks"; // Terminate the script
};

// Wait until the unit starts running
waitUntil {
  sleep 2; // Sleep to save CPU cycles
  _unitSpeed = speed _unit; // Get the speed of the unit
  _unitSpeed > 10; // Check against a value (10 is quite good for running)
};

// If the unit hasn't left tactical pace from another script do it here
if(getAnimSpeedCoef _unit > 1) then {
  _unit setAnimSpeedCoef _animCoef; // "Leave" tactical pace
  _unit setCustomAimCoef _swayCoef; // Decrease the sway
  _unit setUnitRecoilCoefficient _recoilCoef; // Decrease the recoil
};
