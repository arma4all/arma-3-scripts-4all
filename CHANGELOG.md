# Version 0.0.2
## Scripts
### Tactical Pace
- tacticalPace.sqf
  - Script to increase a unit's walking speed (to be used as an additional tactical pace for CQB)

## Functions
All previously implemented scripts have been implemented as functions too. In every folder there is a *functions.hpp* which should be included if you intend to use the functions (or copy and paste it). Otherwise you should modify it according to your needs.

## General purpose
- fn_emptyContainer.sqf
  - Function to empty containers
- fn_openCloseDoor.sqf
  - Function to open and close a building door
- fn_timeMuls.sqf
  - Function to get time multipliers and times for day and night of specified duration

## Dead Bodies
- fn_calcFoV.sqf
  - Function to calculate vectors parallel to the Field-of-View edges
- fn_bodiesInFoV.sqf
  - Function to find the bodies in the Field-of-View of a unit
- fn_bodiesInFoVInternal.sqf
  - Function to find the bodies in the Field-of-View of a unit
  - isInFoV is an internal dependency
- fn_isInFoV.sqf
  - Function to check if an object is in the Field-of-View of a unit
- fn_isInLoS.sqf
  - Function to check if an object is in the Line-of-Sight of a unit
- fn_isInLoSInternal.sqf
  - Function to check if an object is in the Line-of-Sight of a unit
  - isInFoV is an internal dependency

## Medical training
### ACE 3
- fn_inflictWounds.sqf
  - Function to inflict wounds to players
  - It is used with a marker or trigger
  - If called on self makes sure the player won't stay unconscious if inside the area
  - Heal player if he leaves the area
  - If the player dies he is respawn at the location of death bypassing the respawn timer
- fn_fullHeal.sqf
  - Function to fully heal player

### Templates
- fn_exampleTemplate.sqf
  - An example of a function to serve as a guide for coding practices
- fn_functionTemplate.sqf
  - An empty (filled with comments) file with the template of a function to serve as a starting point

### Tactical pace
- fn_tacticalPace.sqf
  - A function to increase a unit's walking speed (to be used as an additional tactical pace for CQB)

### Trigger-used
- fn_addRemoveActions.sqf
  - Function to add/remove actions to/from players entering/leaving an area

## PvEvP framework
This is a sector control mission framework, that allows the user to set a mission on any map. For more information consult its documentation
- description.ext
- initServer.sqf
- PvEvP_functions.hpp
- Documentation
  - PvEvP Framework Documentation.pdf
- Flags
- PvEvP_functions
  - fn_checkSec.sqf
  - fn_handleScore.sqf
  - fn_handleTime.sqf
  - fn_init.sqf
  - fn_run.sqf
  - fn_secCont.sqf
  - fn_timeMuls.sqf
  - fn_warmUp.sqf

# Version 0.0.1
As this is the initial version of the collection, all the scripts listed here are the scripts the collection started with.

## Dead Bodies Scripts
- bodiesInFoV.sqf
  - Script to find dead bodies in the Field-of-View of a unit up to a maximum distance
- bodiesInFoVIntenal.sqf
  - Identical to bodiesInFoV.sqf but internally uses isInFoV.sqf
- calcFoV.sqf
  - Script to calculate directional vectors pointing at the direction of the edges of a unit's Field-of-View
- isInFoV.sqf
  - Script to check if an object is inside the Field-of-View of a unit
- isInLoS.sqf
  - Script to check if an object is inside the Line-of-Sight of a unit (a unit can see it)
- isInLosInternal.sqf
  - Identical to isInLoS but internally uses isInFoV.sqf

## General Purpose Scripts
- emptyContainer.sqf
  - Script to empty containers
- openCloseDoor.sqf
  - Script to open and close a building door

## Medical training scripts
### ACE 3
- fullHeal.sqf
  - Script to fully heal player
- inflictWounds.sqf
  - Script to inflict wounds to players
  - It is used with a marker or trigger
  - If called on self makes sure the player won't stay unconscious if inside the area
  - Heal player if he leaves the area
  - If the player dies he is respawn at the location of death bypassing the respawn timer
- fullHeal.sqf
  - Script to fully heal player

## Templates
- exampleTemplate.sqf
  - An example of a script to serve as a guide for coding practices
- scriptTemplate.sqf
  - An empty (filled with comments) file with the template of a script to serve as a starting point

## Trigger-used
- addRemoveActions.sqf
  - Script to add/remove actions to/from players entering/leaving an area
