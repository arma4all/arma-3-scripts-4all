# How to contribute
First of all, thank you for taking the time to contribute to this collection. We've tried to make a stable release with generic scripts for universal use. We try to fix bugs, add new features and improve functionality and efficiency continuously. You can help us do more.

## Getting started
### Check out the roadmap
Although the collection is a, somewhat random, compilation of scripts, we do have some features in mind and we have issued them. If there is a bug or a feature that is not listed in the **issues** page or there is no one assigned to the issue, feel free to fix/add it! Please consider discussing it in the issue, or create a new issue for it, so there is no conflicting code.

### Writing some code!
Contributing to a project on GitLab is pretty straight forward. If this is your first time, these are the steps you should take.

- Fork this repo.

And that's it! Read the code available and change the part you don't like! Your change should not break the existing code and should pass work as stable as possible.

If you're adding a new script/function/feature, start from the latest branch. It would be a better practice to create a new branch and work in there. This is our standard practice and we really encourage you to follow it.

When you're done, submit a pull or merge (for branches) request and for one of the maintainers to check it out. We would let you know if there is any problem or any changes that should be considered.

A template for new scripts is available in the repository and can be found in the folder named **templates**. It is advisable to start coding new scripts from that as this will save a lot of time for you and the code structure will be quite consistent across the scripts.

### Tests
We haven't prepared any tests, but please spend some time testing your fixes/additions before pulling a pull/merge request. This could potentially save a lot of people some trouble.

### Documentation

Every chunk of code that may be hard to understand has some comments above (or next to) it. If you write some new code or change any part of existing code in a way it would not be functional without changing it's usage, it needs to be documented.
Overcommented code is acceptable and sometimes this is even considered a plus for a script (as some of its users may be beginners). On the other hand undercommented code is not acceptable and should be "fixed". It would be beneficial to add comments yourself, but, most often than not, a maintainer may take on this task.
