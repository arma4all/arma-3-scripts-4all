// Get variables
private _sec = _this select 0; // The sector coordinates
private _gameMode = _this select 1; // Get the game mode
private _isALiVE = _this select 2; // Is ALiVE profile system loaded

// Get the units in the sector
_inSec = allUnits inAreaArray[_sec, 50, 50, 0, false, 15];

// Initialise variables
private _bluSec = west countSide _inSec; // BluFor units in sector
private _opSec = east countSide _inSec; // OpFor units in sector
private _indSec = 0; // Initialise IndFor variable

// Check game mode to see if we have to count IndFor
if(_gameMode == 1) {
  _indSec = independent countSide _inSec; // IndFor spawned units in sector
};

// Check for ALiVE
if(_isALiVE) then {
  // Check for PvEvP
  if(_gameMode == 1) exitWith {
    _indSec = _indSec + count ([_sec, 50, ["GUER"]] call ALIVE_fnc_getNearProfiles); // Add the profiles to the IndFor number
  };

  // If above comparison failed we are playing PvE/PvP
  _opSec = _opSec + count ([_sec, 50, ["EAST"]] call ALIVE_fnc_getNearProfiles); // Add the profiles to the OpFor number
};

// Check for dominant side (reuse _inSec)
_inSec = civilian; // Initialise result

// Make sure one side has more units than the others
if(!(_bluSec == _opSec && {_opSec == _indSec})) then {
  // Check for BluFor domination
  if(_bluSec > _opSec && {_bluSec > _indSec}) exitWith {
    _inSec = west;
  };

  // Check for OpFor domination
  if(_opSec > _bluSec && {_opSec > _indSec}) exitWith {
    _inSec = east;
  };

  // Check for independent domination
  if(_indSec > _bluSec && {_indSec > _opSec}) exitWith {
    _inSec = independent;
  };
};

// Return side
_inSec
