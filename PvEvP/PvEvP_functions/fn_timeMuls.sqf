/* ----------------------------------------------------------------------------------------------------
 * Inputs
 * ----------
 * dayDur [Number] (Optional): Duration of the whole day in real hours (defaults to 24)
 * val [Number] (Optional): The daylight time (direct method) or the day-to-night ratio
 *                          (fractional method) (defaults to 1)
 * dayOfYear﻿ [Date] (Optional): The day of the year for which the multipliers will be
 *                              calculated (defaults to the current ArmA date)
 * method [Boolean] (Optional): Which method to use for the calculation. True is direct
 *                              and false is fractional
 * ----------------------------------------------------------------------------------------------------
 * Output
 * ----------
 * data [Array]: - _this select 0 [Number]: Day multiplier
 *               - _this select 1 [Number]: Night multiplier
 *               - _this select 2 [Number]: Sunrise time
 *               - _this select 3 [Number]: Sunset time
 *
 * ----------------------------------------------------------------------------------------------------
 */

// Get input variables
params[["_dayDur", 24, [24]], // Total duration of day
       ["_val", 1, [1]], // Day-to-Night factor
       ["_dayOfYear", date, [date]], // Asked date
       ["_method", 0, [0]]]; // Method

// Declare some variables﻿
private _riseSet = _dayOfYear call BIS_fnc_sunriseSunsetTime; // Get the sunrise and sunset times of the day
private _durs = [nil, nil]; // Day and night durations

// Calculate day and nigh﻿t duration (in real life hours)
_durs set[0, (_riseSet select 1) - (_riseSet select 0)]; // Calculate day duration
_durs set[1, 24 - (_durs select 0)]; // Calculate night duration

// Check method and calculate
private _dMul = 1; // Initialise day multiplier
private _nMul = 1; // Initialise night multiplier

if(_method == 1) then { // Direct method
  if(_val >= _dayDur) then {
    _dMul = 0.1; // Set minimum day multiplier
    _nMul = 120; // Set maximum night multiplier
  } else {
    _dMul = (_durs select 0)/_liteDur; // Get the day multiplier
    _nMul = (_durs select 1)/(_dayDur - _liteDur); // Get the night multiplier
  };
};

// Fractional method
if(_method == 2) then {
  /* Solve simultaneously:
   *
   * dayDur * dayMul + nightDur * nightMul = 24 (1)
   * (dayDur * dayMul)/(nightDur * nightMul) = dayNightFrac (2)
   *
   * The result is:
   *
   * dayMul = 24/(nightDur * (1 + dayNightFrac))
   * nightMul = (nightDur * dayNightFrac * dayMul)/dayDur
   */
  _dMul = 24/((_durs select 1) * (1 + _val)); // Calculate the day multiplier
  _nMul = ((_durs select 1) * _val * _dMul)/(_durs select 0); // Calculate the night multiplier
};

// Multiply with "global multiplier"
_dMul = _dMul * 24/_dayDur; // Final day multiplier
_nMul = _dMul * 24/_dayDur; // Final night multiplier

// Return and exit
[_dMul, _nMul, _riseSet select 0, _riseSet select 1]
