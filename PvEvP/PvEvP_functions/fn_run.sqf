// Get parameters
private _gameMode = "gameMode" call BIS_fnc_getParamValue; // Get the game mode
private _capTime = "capTime" call BIS_fnc_getParamValue; // Get the capture time
private _endScore = "endScore" call BIS_fnc_getParamValue; // Get the end score
private _mapDom = "mapDom" call BIS_fnc_getParamValue; // Get the domination mode
private _isALiVE = "useALiVE" call BIS_fnc_getParamValue; // Find out if ALiVE is to be used
private _tickDur = "tickDur" call BIS_fnc_getParamValue; // How often to check sectors and handle score
private _secPos = _this select 0; // The sectors to check

// If they want to use ALiVE check that it is loaded
if(_isALiVE) then {
  _isALiVE = ((activatedAddons findIf {_x isEqualTo "alive_sys_profile"}) != -1); // Find out if ALiVE profile system is loaded
};

// Define parameters
private _play = true; // Flag parameter to end mission

// Start score handling
private _finito = [_mapDom] spawn PvEvP_fnc_handleScore;

// Run "for-ever"
while {!(scriptDone _finito)} do {
  // Check the sectors for units
  {
    // Check the sector for dominant side
    private _domSide = [_x, _gameMode, _isALiVE] call PvEvP_fnc_checkSec; // Get the dominant side

    // Check if sector is contested
    if(!(_domSide isEqualTo (PvEvP_secSide select _forEachIndex)) && {!(PvEvP_secCont select _forEachIndex)} && {!(_domSide isEqualTo civilian)}) then {
      secCont set[_forEachIndex, true]; // Set the sector as "being contested"
      [_x, _domSide, _forEachIndex, _capTime, _gameMode, _isALiVE] spawn PvEvP_fnc_secCont; // Start sector contest
    };
  } forEach _secPos;

  // Wait for the duration set by the user before checking again
  uiSleep _tickDur;
};

// Finish mission
// Zero out the losing side's score to make sure the game will end correctly
if((PvEvP_sideScore select 0) > (PvEvP_sideScore select 1)) then {
  // There are BluFor players
  if((playersNumber west) > 0) then {
    east addScoreSide -(scoreSide east); // Zero out OpFor's score

    "SideScore" call BIS_fnc_endMissionServer; // End the mission
  } else {// There aren't any BluFor players
    // In this case there are players only on the losing side, so call everyone lost ending
    "EveryoneLost" call BIS_fnc_endMissionServer; // Terminate mission
  };
} else {
  // There are OpFor players
  if((playersNumber east) > 0) then {
    west addScoreSide -(scoreSide west); // Zero out BluFor's score

    "SideScore" call BIS_fnc_endMissionServer; // End the mission
  } else {// There aren't any OpFor players
    // In this case there are players only on the losing side, so call everyone lost ending
    "EveryoneLost" call BIS_fnc_endMissionServer; // Terminate mission
  };
};
