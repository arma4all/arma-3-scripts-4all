// Get parameters
private _scoreTime = "scoreTime" call BIS_fnc_getParamValue; // Get the score register interval
private _gameTime = "gameTime" call BIS_fnc_getParamValue; // Get the game timer
private _mapDom = _this select 1; // Map domination mode

// Run until end score or timer is reached
while{time < _gameTime} do {
  private _bluDom = {_x isEqualTo west} count PvEvP_secSide; // BluFor occupied sectors
  private _opDom = {_x isEqualTo east} count PvEvP_secSide; // OpFor occupied sectors

  // Check domination mode
  if(_mapDom == -1) then { // Domination = off
    PvEvP_sideScore set[0, (PvEvP_sideScore select 0) + _bluDom]; // Increase the score of the BluFor
    PvEvP_sideScore set[1, (PvEvP_sideScore select 1) + _opDom]; // Increase the score of OpFor
  } else {
    // Check domination mode
    if(_mapDom == 0) then { // No independent
      // Get the difference of the occupied sectors
      private _domDiff = _bluDom - _opDom; // Doesn't matter which is higher (see below)

      // Check who has the most sectors
      if(_bluDom > _opDom) then {
        PvEvP_sideScore set[0, (PvEvP_sideScore select 0) + _domDiff]; // Increase the BluFor score
      } else {
        PvEvP_sideScore set[1, (PvEvP_sideScore select 1) - _domDiff]; // Increase OpFor score (if _opDom is greater _domDiff is negative)
      };
    } else { // Complete domination
      private _indDom = {_x isEqualTo independent} count secSide; // Independent occupied sectors

      // Check BluFor domination
      if(_bluDom > _opDom && {_bluDom > _indDom}) exitWith {
        private _domDiff = _bluDom - (_opDom max _indDom); // Find the sector number superiority
        PvEvP_sideScore set[0, (PvEvP_sideScore select 0) + _domDiff]; // Increase BluFor score
      };

      if(_opDom > _bluDom && {_opDom > _indDom}) exitWith {
        private _domDiff = _opDom - (_bluDom max _indDom); // Find the sector number superiority
        PvEvP_sideScore set[1, (PvEvP_sideScore select 1) + _domDiff]; // Increase OpFor score
      };
    };
  };

  // Check for end conditions and force mission to end
  if((PvEvP_sideScore select 0) >= _endScore || {(PvEvP_sideScore select 1) >= _endScore}) exitWith {};

  // If the above conditions are not met wait for the score register duration
  uiSleep _scoreTime;
};
