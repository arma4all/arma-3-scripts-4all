// Get parameters
private _numOfObj = "numOfObj" call BIS_fnc_getParamValue; // Number of objectives
private _useFlag = "customFlag" call BIS_fnc_getParamValue; // See whether we should use custom flags
private _gameMode = "gameMode" call BIS_fnc_getParamValue; // Get the game mode
private _isALiVE = "useALiVE" call BIS_fnc_getParamValue; // Check that ALiVE profiling system is used

// If they want to use ALiVE check that it is loaded
if(_isALiVE) then {
  _isALiVE = ((activatedAddons findIf {_x isEqualTo "alive_sys_profile"}) != -1); // Find out if ALiVE profile system is loaded
};

// Declare other global variables
PvEvP_secSide = []; // Initialise the array holding the side of each sector
PvEvP_secCont = []; // Sector being contested flags
PvEvP_sideScore = [0, 0]; // The score of each side
PvEvP_flags = [nil, nil, nil]; // Initialise the flag paths array

// Initialise variables
private _secPos = []; // Initialise the array to hold the sector coordinates

// Fill the flags array
if(_useFlag) then {
  PvEvP_flags set[0, "Flags\BlueFlag.jpg"];
  PvEvP_flags set[1, "Flags\RedFlag.jpg"];
  PvEvP_flags set[2, "Flags\GreenFlag.jpg"];
} else {
  PvEvP_flags set[0, "\A3\Data_F\Flags\Flag_nato_CO.paa"];
  PvEvP_flags set[1, "\A3\Data_F\Flags\Flag_CSAT_CO.paa"];
  PvEvP_flags set[2, "\A3\Data_F\Flags\Flag_FIA_CO.paa"];
};

// Go through the objects placed at the sectors (in 3den Editor)
for "_i" from _numOfObj to 1 step - 1 do {
  // Create the name of the variable
  private _flag = missionNamespace getVariable["PvEvP_sec_" + (str _i), objNull]; // Get the variable holding the object
  private _flagPos = getPosATL _flag; // Get the position of the flag

  // Add animation event handlers to the flags
  [_flag, "FlagAnimationDone", {
    if(_phaseEnd == 0) then {
      _flag setFlagTexture ""; // Take it down (no flag pole)
    };
    }] call BIS_fnc_addScriptedEventHandler;

  // Get the position of the flag
  _secPos set[_i - 1, _flagPos]; // Get the position of the object placed in the sector

  // Get the dominant side
  private _domSide = [_flagPos, _gameMode, _isALiVE] call PvEvP_fnc_checkSec;

  // Check for dominant side
  if(!(_domSide isEqualTo civilian)) then {
    PvEvP_secSide set[_i - 1, _domSide]; // Set sector dominant side

    // Raise their flag
    switch(_domSide) do {
      case west: {_flag setFlagTexture (PvEvP_flags select 0)};
      case east: {_flag setFlagTexture (PvEvP_flags select 1)};
      case independent: {_flag setFlagTexture (PvEvP_flags select 2)};
    };
  } else {
    PvEvP_secSide set[_i - 1, civilian]; // Set sector to neutral
    _flag setFlagTexture ""; // Make sure there's no flag
    _flag setFlagAnimationPhase 0; // Set the animation phase to zero (flag at the bottom)
  };

  PvEvP_secCont set[_i, false]; // Initialise flags to "not being contested"
};

// Add base flags
PvEvP_baseBlue setFlagTexture (PvEvP_flags select 0); // Set BluFor base flag
PvEvP_baseRed setFlagTexture (PvEvP_flags select 1); // Set OpFor base flag

_secPos
