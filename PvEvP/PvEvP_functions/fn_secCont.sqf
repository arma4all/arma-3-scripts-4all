// Get variables
private _secPos = _this select 0; // The sector coordinates
private _contSide = _this select 1; // Side contesting the sector
private _idx = _this select 2; // Index of the sector
private _capTime = _this select 3; // Get the capture time
private _gameMode = _this select 4; // Get the game mode
private _isALiVE = _this select 5; // Is ALiVE profile system loaded

private _capted = false; // Variable to check if sector is captured
private _flag = missionNamespace getVariable ["PvEvP_sec_" + (str _idx), objNull]; // Get the flag of the sector

// Run until captured or dominant side has changed
while{!_capted} do {
  // Declare variables
  private _timer = _capTime; // Initialise timer

  // Run the timer to capture OR neutralise
  while{_timer > 0} do {
    // Check the sector for dominant side
    private _domSide = [_secPos, _gameMode, _isALiVE] call PvEvP_fnc_checkSec; // Get the dominant side

    // Check if still contested
    if(_domSide isEqualTo _contSide) then {
      _timer = _timer - 1; // Decrease the timer by one

      // Sleep for 1 second
      uiSleep 1;
    } else {
      // If dominant side has changed
      _timer = -1; // Force inner while loop exit
      _capted = true; // Force outer while loop exit
    };
  };

  // Check if timer finished successfully
  if(_timer == 0) then {
    // Check captured or neutralised
    if((PvEvP_secSide select _idx) isEqualTo civilian) then {
      PvEvP_secSide set[_idx, _contSide]; // Set the sector as captured
      _capted = true; // Set this as captured to exit outer while loop

      // Check which side just captured (or neutralised) the sector
      switch(_contSide) do {
          case west: {_flag setFlagTexture (PvEvP_flags select 0)};
          case east: {_flag setFlagTexture (PvEvP_flags select 1)};
          case independent: {_flag setFlagTexture (PvEvP_flags select 2)};
      };

      // Animate flag
      [_flag, 1, false] call BIS_fnc_animateFlag; // Move it up
    } else {
      // Set the sector as neutralised
      PvEvP_secSide set[_idx, civilian];

      // Animate flag (will be taken off in the assigned scriptedEventHandler)
      [_flag, 0, false] call BIS_fnc_animateFlag; // Move it down
    };
  };
};

// Set the sector as "not contested"
PvEvP_secCont set[_idx, false];
