// Get parameters
private _dayDur = "dayDur" call BIS_fnc_getParamValue; // Get the duration of the day
private _liteDur = "lightRur" call BIS_fnc_getParamValue; // Get the duration of light time
private _dnFrac = "dnFrac" call BIS_fnc_getParamValue; // Get the day-to-night fration
private _timeMode = "timeMode" call BIS_fnc_getParamValue; // Get the mode of the time manipulation

// Check if we are asked to mess with time
if(_timeMode == -1) exitWith {};

// Check for constant time multiplier
if(_timeMode == 0) exitWith {
  // Set time multiplier and leave function
  setTimeMultiplier (([_dayDur, _dnFrac, date, _timeMode] call PvEvP_fnc_timeMuls) select 0);
};

// Define some parameters
private _timeVals = [nil, nil, nil, nil]; // Initialise values array

// Get the values of the multipliers and the sunrise and sunset
_timeVals = [_dayDur, _dnFrac, date, _timeMode] call PvEvP_fnc_timeMuls;

// Define some variables
private _dayCheck = 0; // Variable to check if we have taken the sunrise and sunset values of the current day

// Loop "for ever"
while{true} do {
  private _time = daytime; // Get the current (ArmA) time

  // Check if the day has passed and we have to acquire new values for the day
  if(_time < _dayCheck) then {
    _timeVals = [_dayDur, _liteDur, date, true] call PvEvP_fnc_timeMuls; // Get new values
    _dayCheck = _time; // Update check value
  } else {
    _dayCheck = _time; // Update check value
  };

  // Handle time multipliers
  if(_time >= (_timeVals select 3) || {_time < (_timeVals select 2)}) then {
    setTimeMultiplier (_timeVals select 1); // Set the night time multiplier
  } else {
    setTimeMultiplier (_timeVals select 0); // Set the day time multiplier
  };

  // Sleep (a lot...)
  uiSleep 120; // Sleep one second (adjust value as needed)
};
