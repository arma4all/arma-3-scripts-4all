// Get parameters
private _warmUp = "warmUp" call BIS_fnc_getParamValue; // Warm up time
private _baseLoc = [nil, nil]; // Location of bases

// Find when to stop the warmup
_warmUp = _warmUp * 60; // Add the time this function was called to the warm up time

// Get base locations
_baseLoc set[0, getPosATL PvEvP_baseBlue]; // BluFor base location
_baseLoc set[1, getPosATL PvEvP_baseRed]; // OpFor base locaation

// Handle the AI first
private _AI = allUnits - allPlayers - (entities "HeadlessClient_F"); // Get all AI units
private _isALiVE = ((activatedAddons findIf {_x isEqualTo "alive_sys_profile"}) != -1); // Find out if ALiVE profiling system is loaded

// Stop them from moving
_nil = [_AI, _isALiVE] spawn {
  // Run through all the AIs
  {
    _x disableAI "Move"; // Make sure they'll stay put
  } forEach (_this select 0);

  // Handle profiled units
  if(_isALiVE) then {
    ["ALiVE_sys_profile"] call ALiVE_fnc_pauseModule; // Stop the AI profiling system module's simulation
  };
};

// Run for the warm up time
while{time < _warmUp} do {
  // Get all units
  private _humans = allPlayers - (entities "HeadlessClient_F"); // Get players

  // Handle BluFor
  private _curSide = _units select {side _x == west}; // Get all the BluFor units
  private _outLaws = _curSide inAreaArray[_baseLoc select 0, 50, 50, 0, false, 10]; // Find those that are in base
  _outLaws = _curSide - _outLaws; // Find those that are out of the base

  // Restore order
  {
    _x setPos (_baseLoc select 0);
  } forEach _outLaws;

  // Handle OpFor
  _curSide = _units select {side _x == east}; // Get all OpFor units
  _outLaws = _curSide inAreaArray[_baseLoc select 1, 50, 50, 0, false, 10]; // Find those that are in base
  _outLaws = _curSide - _outLaws; // Find those that are out of the base

  // Restore order
  {
    _x setPos (_baseLoc select 1);
  } forEach _outLaws;

  // Rest for a while
  uiSleep 5;
};

// Release AI
_nil = [_AI, _isALiVE] spawn {
  // Run through all the AIs
  {
    _x enableAI "Move"; // Free free, set them free...!!!
  } forEach (_this select 0);

  // Handle profiled units
  // Handle profiled units
  if(_isALiVE) then {
    ["ALiVE_sys_profile"] call ALiVE_fnc_unPauseModule; // Re-enable the AI profiling system module's simulation
  };
};
