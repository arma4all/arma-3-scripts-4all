// Create the class holding the functions information
class PvEvP {
  tag = "PvEvP"; // The functions' tag

  class functions {
    file = "PvEvP_functions"; // The file holding the functions

    // The functions
    class init {
      Description = "Initialise the variables of needed for the mission"; // Description of the function
    };

    class run {
      Description = "The main function of the mission that constantly checks the sectors"; // Description of the function
    };

    class checkSec {
      Description = "Check sectors and return the dominant side in the sector"; // Description of the function
    };

    class secCont {
      Description = "Handle the sectors that are being contested"; // Description of the function
    };

    class handleTime {
      Description = "Handle time multipliers"; // Description of the function
    };

    class handleScore {
      Description = "Function to handle the score of the mission"; // Description of the function
    };

    class timeMuls {
      Description = "Function to take the time multiplier values"; // Description of the function
    };

    class warmUp {
      Description = "Handler for the warm up time"; // Description of the function
    };
  };
};
