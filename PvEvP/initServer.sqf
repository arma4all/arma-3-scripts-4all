[] spawn PvEvP_fnc_warmUp;

// Handle time
[] spawn PvEvP_fnc_handleTime;

// Initialise the sectors
private _secPos = call PvEvP_fnc_init;

// Run the script of the mission
[_secPos] spawn PvEvP_fnc_run;
