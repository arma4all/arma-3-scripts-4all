# Functions
Here you can find all the functions divided into categories and a brief explanation of their functionality. For more information about their use you can see inside the function files (look at the comments at the top of the file).

The functions included in the collection are:

- Dead Bodies
  - functions.hpp
    - File needed to be able to use the functions
  - fn_bodiesInFoV.sqf
    - Function to find the bodies in the Field-of-View of a unit
  - fn_bodiesInFoVIntenal.sqf
    - Function to find the bodies in the Field-of-View of a unit (fn_isInFoV.sqf is an internal dependency)
  - fn_calcFoV.sqf
    - Function to calculate the vectors parallel to the boundaries of the Field-of-View of a unit
  - fn_isInFoV.sqf
    - Function to check if an object is in the Field-of-View of a unit
  - fn_isInLoS.sqf
    - Function to check if an object is in the Line-of-Sight of a unit
  - fn_isInLosInternal.sqf
    - Function to check if an object is in the Line-of-Sight of a unit (fn_isInFoV is an internal dependency)
- General Purpose
  - functions.hpp
    - File needed to be able to use the functions
  - fn_emptyContainer.sqf
    - Script to empty a container
  - fn_openCloseDoor.sqf
    - Script to open and close doors
  - fn_timeMuls.sqf
    - Function that takes as arguments the full duration of a day, the day-to-night ratio
      and day of year and returns the appropriate time multipliers and sunrise and sunset times
- Medical
  - ACE 3
    - functions.hpp
      - File needed to be able to use the functions
    - fn_fullHeal.sqf
      - Function to fully heal player
    - fn_inflictWounds.sqf
      - Function to inflict wounds to players
      - If called on self makes sure the player won't stay unconscious if inside the medical training area
      - Heal player if leaves the medical training area
      - If the player dies he is repsawned at the location of death bypassing the repsawn timer
- Tactical pace
  - functions.hpp
    - File needed to be able to use the functions
  - fn_tacticalPace.sqf
    - Script to increase the walking speed of a unit
    - It is meant to be used as an additional tactical pace (mostly) for CQB purpose
- Trigger-Used
  - functions.hpp
    - File needed to be able to use the functions
  - fn_addRemoveActions.sqf
    - Function to add/remove actions to/from players entering/leaving an area
- Templates
  - fn_exampleTemplate.sqf
    - Example of a function using the template
  - fn_functionTemplate.sqf
    - Template for functions
