# Credits
Here you can find credits to all contributors, direct or indirect.

# Info
**Contributors** are those who have directly contributed to the development of the scripts. Direct contribution may be either writing of even one line of code, correction of some kind (even an underscore correction is considered a direct contribution), or even active discussion on possible corrections, directions, or suggestions.

# Contributors
- **Aris**
  - Kick-started the whole project
  - Contributed scripts
    - emptyContainer.sqf
    - openCloseDoor.sqf
- **Achilles**
  - Kick-started the whole project
  - Project maintainer
  - Contributed scripts
    - addRemoveActions.sqf
    - emptyContainer.sqf
    - openCloseDoor.sqf
    - bodiesInFoV.sqf
    - bodiesInFoVInternal.sqf
    - calcFoV.sqf
    - isInFoV.sqf
    - isInLos.sqf
    - isInLosInternal.sqf
    - exampleTemplate.sqf
    - scriptTemplate.sqf
    - fullHeal.sqf
    - inflictWounds.sqf
    - tacticalPace.sqf
  - Contributed functions
    - fn_addRemoveActions.sqf
    - fn_emptyContainer.sqf
    - fn_openCloseDoor.sqf
    - fn_inflictWounds.sqf
    - fn_fullHeal.sqf
    - fn_bodiesInFoV.sqf
    - fn_bodiesInFoVInternal.sqf
    - fn_calcFoV.sqf
    - fn_isInFoV.sqf
    - fn_isInLoS.sqf
    - fn_isInLoSInternal.sqf
    - fn_exampleTemplate.sqf
    - fn_functionTemplate.sqf
    - fn_addRemoveActions.sqf
    - fn_tacticalPace.sqf
    - fn_timeMuls.sqf
    - functions.hpp (for the above functions)
  - Kick-started the PvEvP framework
