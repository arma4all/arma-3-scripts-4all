# ArmA 3 Scripts 4All
This is a collection of ArmA 3 scripts.

We do use a version number for the collection but it is just for "timeline" purposes, and to mark updates and improvements to the content.

The current version is **version 0.0.2**.

## Disclaimer
The scripts are COMPLETELY free of copyrights and warranties and can be used in ANY way anyone sees fit. This means you can copy them, adapt them to your needs, put them somewhere online, get money for them, give money for them, don’t give a shit about them, or literally do whatever you want with them. No script author is held responsible for any positive or negative outcome from the use of the available scripts. As there are no warranties or copyrights, no credits and references are needed.

For more information see **LICENSE**.

## Information
The scripts included in the collection comprise a somewhat random compilation of scripts created by the contributors. We have tried to impose a common style for the code and the way scripts are structured and used, but this may not always be true.

## Scripts
The scripts are divided into various categories and sub-categories that we thought make sense in some way. Nevertheless some scripts may be used in various circumstances and so they may fit under more than one category. If a script is generic enough it will be placed under the _General_ category.

The categories and the scripts under them are:

- Dead Bodies
  - bodiesInFoV.sqf
  - bodiesInFoVIntenal.sqf
  - calcFoV.sqf
  - isInFoV.sqf
  - isInLoS.sqf
  - isInLosInternal.sqf
- General Purpose
  - emptyContainer.sqf
  - openCloseDoor.sqf
- Medical
  - ACE 3
    - fullHeal.sqf
    - inflictWounds.sqf
- Templates
  - exampleTemplate.sqf
  - scriptTemplate.sqf
- Tactical pace
  - tacticalPace.sqf
- Trigger-Used
  - addRemoveActions.sqf

## Functions
Most (if not all) scripts are also implemented as functions and are divided into the same categories. In every folder there is a *functions.hpp* which should be included if you intend to use the functions (or copy and paste it). Otherwise you should modify it according to your needs. So, the available functions are:

- Dead Bodies
  - fn_bodiesInFoV.sqf
  - fn_bodiesInFoVIntenal.sqf
  - fn_calcFoV.sqf
  - fn_isInFoV.sqf
  - fn_isInLoS.sqf
  - fn_isInLosInternal.sqf
- General Purpose
  - fn_emptyContainer.sqf
  - fn_openCloseDoor.sqf
  - fn_timeMuls.sqf
- Medical
  - ACE 3
    - fn_inflictWounds.sqf
    - fn_fullHeal.sqf
- Templates
  - fn_exampleTemplate.sqf
  - fn_functionTemplate.sqf
- Tactical pace
  - fn_tacticalPace.sqf
- Trigger-Used
  - fn_addRemoveActions.sqf

For more information about the scripts and their use see **SCRIPTS.md**. For functions see **FUNCTIONS.md**.

## PvEvP Framework
This is a sector control mission framework, that allows the user to set a mission on any map. The content of the framework are summarised below. For more information on the framework consult its documentation in the corresponding folder.

- description.ext
- initServer.sqf
- PvEvP_functions.hpp
- Documentation
  - PvEvP Framework Documentation.pdf
- Flags
- PvEvP_functions
  - fn_checkSec.sqf
  - fn_handleScore.sqf
  - fn_handleTime.sqf
  - fn_init.sqf
  - fn_run.sqf
  - fn_secCont.sqf
  - fn_timeMuls.sqf
  - fn_warmUp.sqf
